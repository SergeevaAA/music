﻿namespace Music.Domain
{
    using Music.Domain.Models;
    using Music.Domain.ViewModels;

    /// <summary>
    /// Класс для конвертации списка моделей в список вью-моделей
    /// </summary>
    public static class Converter
    {
        /// <summary>
        /// Конвертер исполнителей
        /// </summary>
        /// <param name="musician">Исполнитель</param>
        /// <returns>Вью-модель исполнителя</returns>
        public static MusicianViewModel ToViewModel(Musician musician)
        {
            return new MusicianViewModel(musician);
        }

        /// <summary>
        /// Конвертер альбомов
        /// </summary>
        /// <param name="album">Альбом</param>
        /// <returns>Вью-модель альбома</returns>
        public static AlbumViewModel ToViewModel(Album album)
        {
            return new AlbumViewModel(album);
        }

        /// <summary>
        /// Конвертер треков
        /// </summary>
        /// <param name="track">Трек</param>
        /// <returns>Вью-модель трека</returns>
        public static TrackViewModel ToViewModel(Track track)
        {
            return new TrackViewModel(track);
        }
    }
}