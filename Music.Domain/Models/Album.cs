﻿namespace Music.Domain.Models
{
    using System.Collections.Generic;

    /// <summary>
    /// Альбом
    /// </summary>
    public class Album
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Название
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Год релиза
        /// </summary>
        public int ReleaseYear { get; set; }

        /// <summary>
        /// Идентификатор исполнителя
        /// </summary>
        public int MusicianId { get; set; }

        /// <summary>
        /// Исполнитель
        /// </summary>
        public Musician Musician { get; set; }

        /// <summary>
        /// Список треков
        /// </summary>
        public List<Track> Tracks { get; set; }

        /// <summary>
        /// Конструктор класса <see cref="Album"/>
        /// </summary>
        public Album()
        {
            Tracks = new List<Track>();
        }
    }
}