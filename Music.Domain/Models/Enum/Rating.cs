﻿namespace Music.Domain.Models.Enum
{
    /// <summary>
    /// Рейтинг
    /// </summary>
    public enum Rating : short
    {
        /// <summary>
        /// Не задан
        /// </summary>
        NotSet = 0,

        /// <summary>
        /// Понравилось
        /// </summary>
        Liked = 1,

        /// <summary>
        /// Не понравилось
        /// </summary>
        Disliked = 2
    }
}