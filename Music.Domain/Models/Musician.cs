﻿namespace Music.Domain.Models
{
    using System.Collections.Generic;

    /// <summary>
    /// Исполнитель
    /// </summary>
    public class Musician
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Возраст
        /// </summary>
        public int Age { get; set; }

        /// <summary>
        /// Жанр
        /// </summary>
        public string Genre { get; set; }

        /// <summary>
        /// Год начала карьеры
        /// </summary>
        public int CareerStartYear { get; set; }

        /// <summary>
        /// Список альбомов
        /// </summary>
        public List<Album> Albums { get; set; }

        /// <summary>
        /// Конструктор класса <see cref="Musician"/>
        /// </summary>
        public Musician()
        {
            Albums = new List<Album>();
        }
    }
}