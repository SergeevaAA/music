﻿namespace Music.Domain.Models
{
    using Music.Domain.Models.Enum;
    using System;

    /// <summary>
    /// Трек
    /// </summary>
    public class Track
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Название
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Длительность
        /// </summary>
        public TimeSpan Duration { get; set; }

        /// <summary>
        /// Находится ли в избранном
        /// </summary>
        public bool IsFavourite { get; set; }

        /// <summary>
        /// Прослушан ли
        /// </summary>
        public bool IsListened { get; set; }

        /// <summary>
        /// Рейтинг
        /// </summary>
        public Rating Rating { get; set; }

        /// <summary>
        /// Идентификатор альбома
        /// </summary>
        public int AlbumId { get; set; }

        /// <summary>
        /// Альбом
        /// </summary>
        public Album Album { get; set; }
    }
}