﻿namespace Music.Domain
{
    using Microsoft.EntityFrameworkCore;
    using Music.Domain.Models;

    /// <summary>
    /// Контекст для получения данных из БД
    /// </summary>
    public class MusicContext : DbContext
    {
        /// <summary>
        /// Конструктор класса <see cref="MusicContext"/>
        /// </summary>
        /// <param name="options">Опции</param>
        public MusicContext(DbContextOptions<MusicContext> options) : base(options)
        {
        }

        /// <summary>
        /// Список исполнителей
        /// </summary>
        public DbSet<Musician> Musicians { get; set; }

        /// <summary>
        /// Список альбомов
        /// </summary>
        public DbSet<Album> Albums { get; set; }

        /// <summary>
        /// Список треков
        /// </summary>
        public DbSet<Track> Tracks { get; set; }
    }
}