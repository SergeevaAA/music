﻿namespace Music.Domain.Repository.Interfaces
{
    using System.Collections.Generic;

    /// <summary>
    /// Репозиторий для работы с сущностями типа Т
    /// </summary>
    /// <typeparam name="T">Тип сущности</typeparam>
    public interface IRepository<T>
    {
        /// <summary>
        /// Получение всех сущностей типа Т
        /// </summary>
        /// <returns></returns>
        IEnumerable<T> GetAll();
    }
}