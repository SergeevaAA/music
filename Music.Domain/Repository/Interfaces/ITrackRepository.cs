﻿namespace Music.Domain.Repository.Interfaces
{
    using Music.Domain.Models;
    using System.Collections.Generic;

    /// <summary>
    /// Репозиторий для работы с треками
    /// </summary>
    public interface ITrackRepository : IRepository<Track>
    {
        /// <summary>
        /// Получение всех треков альбома по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор альбома</param>
        /// <returns>Список треков</returns>
        IEnumerable<Track> GetTracksForAlbum(int id);

        /// <summary>
        /// Получение всех треков исполнителя по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор исполнителя</param>
        /// <returns>Список треков</returns>
        IEnumerable<Track> GetTracksForMusician(int id);

        /// <summary>
        /// Изменение статуса "Избранное" для трека
        /// </summary>
        /// <param name="id">Индентификатор трека</param>
        /// <param name="status">Статус</param>
        void SetIsFavouriteStatus(int id, bool status);

        /// <summary>
        /// Изменение статуса "Прослушан" для трека
        /// </summary>
        /// <param name="id">Индентификатор трека</param>
        /// <param name="status">Статус</param>
        void SetIsListenedStatus(int id, bool status);

        /// <summary>
        /// Изменение рейтинга для трека
        /// </summary>
        /// <param name="id">Индентификатор трека</param>
        /// <param name="rating">Рейтинг</param>
        void SetRating(int id, short rating);
    }
}