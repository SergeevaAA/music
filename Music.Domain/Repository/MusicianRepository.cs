﻿namespace Music.Domain.Repository
{
    using Microsoft.EntityFrameworkCore;
    using Music.Domain.Models;
    using Music.Domain.Repository.Interfaces;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Репозиторий для работы с исполнителями
    /// </summary>
    public class MusicianRepository : IRepository<Musician>
    {
        private readonly MusicContext _context;

        /// <summary>
        /// Конструктор класса <see cref="MusicianRepository"/>
        /// </summary>
        /// <param name="context">Контекст подключения к БД</param>
        public MusicianRepository(MusicContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Получение всех исполнителей
        /// </summary>
        /// <returns>Список исполнителей</returns>
        public IEnumerable<Musician> GetAll()
        {
            return _context.Musicians.Include(m => m.Albums).ToList();
        }
    }
}