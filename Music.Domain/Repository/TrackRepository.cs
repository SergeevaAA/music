﻿namespace Music.Domain.Repository
{
    using Music.Domain.Models;
    using Music.Domain.Models.Enum;
    using Music.Domain.Repository.Interfaces;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Репозиторий для работы с треками
    /// </summary>
    public class TrackRepository : ITrackRepository
    {
        private readonly MusicContext _context;

        /// <summary>
        /// Конструктор класса <see cref="TrackRepository"/>
        /// </summary>
        /// <param name="context">Контекст подключения к БД</param>
        public TrackRepository(MusicContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Получение всех треков
        /// </summary>
        /// <returns>Список треков</returns>
        public IEnumerable<Track> GetAll()
        {
            return _context.Tracks.ToList();
        }

        /// <summary>
        /// Получение всех треков альбома по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор альбома</param>
        /// <returns>Список треков</returns>
        public IEnumerable<Track> GetTracksForAlbum(int id)
        {
            return _context.Tracks.Where(t => t.AlbumId == id).ToList();
        }

        /// <summary>
        /// Получение всех треков исполнителя по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор исполнителя</param>
        /// <returns>Список треков</returns>
        public IEnumerable<Track> GetTracksForMusician(int id)
        {
            return _context.Tracks.Where(t => t.Album.MusicianId == id).ToList();
        }

        /// <summary>
        /// Изменение статуса "Избранное" для трека
        /// </summary>
        /// <param name="id">Индентификатор трека</param>
        /// <param name="status">Статус</param>
        public void SetIsFavouriteStatus(int id, bool status)
        {
            var track = _context.Tracks.FirstOrDefault(t => t.Id == id);
            track.IsFavourite = status;
            _context.SaveChanges();
        }

        /// <summary>
        /// Изменение статуса "Прослушан" для трека
        /// </summary>
        /// <param name="id">Индентификатор трека</param>
        /// <param name="status">Статус</param>
        public void SetIsListenedStatus(int id, bool status)
        {
            var track = _context.Tracks.FirstOrDefault(t => t.Id == id);
            track.IsListened = status;
            _context.SaveChanges();
        }

        /// <summary>
        /// Изменение рейтинга для трека
        /// </summary>
        /// <param name="id">Индентификатор трека</param>
        /// <param name="rating">Рейтинг</param>
        public void SetRating(int id, short rating)
        {
            var track = _context.Tracks.FirstOrDefault(t => t.Id == id);
            track.Rating = (Rating)rating;
            _context.SaveChanges();
        }
    }
}