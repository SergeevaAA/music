﻿namespace Music.Domain.Services.Interfaces
{
    using System.Collections.Generic;

    public interface IService<T>
    {
        IEnumerable<T> GetAll();
    }
}