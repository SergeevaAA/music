﻿namespace Music.Domain.Services
{
    using Music.Domain.Models;
    using Music.Domain.Repository.Interfaces;
    using Music.Domain.Services.Interfaces;
    using System.Collections.Generic;

    /// <summary>
    /// Сервис для работы с исполнителями
    /// </summary>
    public class MusicianService : IService<Musician>
    {
        private readonly IRepository<Musician> _repository;

        /// <summary>
        /// Конструктор класса <see cref="MusicianService"/>
        /// </summary>
        /// <param name="repository">Репозиторий</param>
        public MusicianService(IRepository<Musician> repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Получение всех исполнителей
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Musician> GetAll()
        {
            return _repository.GetAll();
        }
    }
}