﻿namespace Music.Domain.Services
{
    using Music.Domain.Models;
    using Music.Domain.Repository.Interfaces;
    using Music.Domain.Services.Interfaces;
    using System.Collections.Generic;

    /// <summary>
    /// Сервис для работы с треками
    /// </summary>
    public class TrackService : ITrackService
    {
        private readonly ITrackRepository _repository;

        /// <summary>
        /// Конструктор класса <see cref="TrackService"/>
        /// </summary>
        /// <param name="repository">Репозиторий</param>
        public TrackService(ITrackRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Получение всех треков
        /// </summary>
        /// <returns>Список треков</returns>
        public IEnumerable<Track> GetAll()
        {
            return _repository.GetAll();
        }

        /// <summary>
        /// Получение всех треков альбома по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор альбома</param>
        /// <returns>Список треков</returns>
        public IEnumerable<Track> GetTracksForAlbum(int id)
        {
            return _repository.GetTracksForAlbum(id);
        }

        /// <summary>
        /// Получение всех треков исполнителя по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор исполнителя</param>
        /// <returns>Список треков</returns>
        public IEnumerable<Track> GetTracksForMusician(int id)
        {
            return _repository.GetTracksForMusician(id);
        }

        /// <summary>
        /// Изменение статуса "Избранное" для трека
        /// </summary>
        /// <param name="id">Индентификатор трека</param>
        /// <param name="status">Статус</param>
        public void SetIsFavouriteStatus(int id, bool status)
        {
            _repository.SetIsFavouriteStatus(id, status);
        }

        /// <summary>
        /// Изменение статуса "Прослушан" для трека
        /// </summary>
        /// <param name="id">Индентификатор трека</param>
        /// <param name="status">Статус</param>
        public void SetIsListenedStatus(int id, bool status)
        {
            _repository.SetIsListenedStatus(id, status);
        }

        /// <summary>
        /// Изменение рейтинга для трека
        /// </summary>
        /// <param name="id">Индентификатор трека</param>
        /// <param name="rating">Рейтинг</param>
        public void SetRating(int id, short rating)
        {
            _repository.SetRating(id, rating);
        }
    }
}