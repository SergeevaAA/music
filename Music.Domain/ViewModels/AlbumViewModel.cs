﻿namespace Music.Domain.ViewModels
{
    using Music.Domain.Models;

    /// <summary>
    /// Альбом
    /// </summary>
    public class AlbumViewModel
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Название
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Год релиза
        /// </summary>
        public int ReleaseYear { get; set; }

        /// <summary>
        /// Конструктор класса <see cref="AlbumViewModel"/>
        /// </summary>
        /// <param name="album">Альбом</param>
        public AlbumViewModel(Album album)
        {
            Id = album.Id;
            Name = album.Name;
            ReleaseYear = album.ReleaseYear;
        }
    }
}