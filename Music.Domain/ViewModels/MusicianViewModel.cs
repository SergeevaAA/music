﻿namespace Music.Domain.ViewModels
{
    using Music.Domain.Models;
    using System;
    using System.Collections.Generic;

    public class MusicianViewModel
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Возраст
        /// </summary>
        public int Age { get; set; }

        /// <summary>
        /// Жанр
        /// </summary>
        public string Genre { get; set; }

        /// <summary>
        /// Год начала карьеры
        /// </summary>
        public int CareerStartYear { get; set; }

        /// <summary>
        /// Список альбомов
        /// </summary>
        public List<AlbumViewModel> Albums { get; set; }

        /// <summary>
        /// Конструктор класса <see cref="MusicianViewModel"/>
        /// </summary>
        /// <param name="musician">Исполнитель</param>
        public MusicianViewModel(Musician musician)
        {
            Id = musician.Id;
            Name = musician.Name;
            Age = musician.Age;
            Genre = musician.Genre;
            CareerStartYear = musician.CareerStartYear;
            Albums = musician.Albums.ConvertAll(new Converter<Album, AlbumViewModel>(Converter.ToViewModel));
        }
    }
}