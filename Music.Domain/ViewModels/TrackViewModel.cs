﻿namespace Music.Domain.ViewModels
{
    using Music.Domain.Models;
    using Music.Domain.Models.Enum;

    /// <summary>
    /// Трек
    /// </summary>
    public class TrackViewModel
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Название
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Длительность
        /// </summary>
        public string Duration { get; set; }

        /// <summary>
        /// Находится ли в избранном
        /// </summary>
        public bool IsFavourite { get; set; }

        /// <summary>
        /// Прослушан ли
        /// </summary>
        public bool IsListened { get; set; }

        /// <summary>
        /// Рейтинг
        /// </summary>
        public Rating Rating { get; set; }

        /// <summary>
        /// Конструктор класса <see cref="TrackViewModel"/>
        /// </summary>
        /// <param name="track">Трек</param>
        public TrackViewModel(Track track)
        {
            Id = track.Id;
            Name = track.Name;
            Duration = string.Format("{0:D2}:{1:D2}", (int)track.Duration.TotalMinutes, track.Duration.Seconds);
            IsFavourite = track.IsFavourite;
            IsListened = track.IsListened;
            Rating = track.Rating;
        }
    }
}