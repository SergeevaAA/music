import Vue from 'vue';
import axios from 'axios';
import TreeMenu from './components/TreeMenu.vue';
import TracksList from './components/TracksList.vue';

Vue.config.productionTip = true;

new Vue({
    el: '#tree-menu',
    data: {
        tree: {}
    },
    mounted() {
        axios.get('https://localhost:44386/api/musicians').then(response => {
            this.tree = response.data
        })
    },
    components: {
        TreeMenu
    }
})

export const tracksVue = new Vue({
    el: '#tracks-list',
    data: {
        tracks: []
    },
    mounted() {
        this.getTracks();
    },
    components: {
        TracksList
    },
    methods: {
        getTracks: function (url = 'https://localhost:44386/api/tracks') {
            axios.get(url).then(response => {
                this.tracks = response.data
            })
        }
    }
})