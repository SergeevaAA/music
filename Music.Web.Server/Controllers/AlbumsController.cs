﻿namespace Music.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.AspNetCore.Mvc;
    using Music.Domain;
    using Music.Domain.Models;
    using Music.Domain.Services.Interfaces;
    using Music.Domain.ViewModels;

    /// <summary>
    /// Контроллер для работы с альбомами
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class AlbumsController : ControllerBase
    {
        private readonly ITrackService _trackService;

        /// <summary>
        /// Конструктор класса <see cref="AlbumsController"/>
        /// </summary>
        /// <param name="trackService">Сервис для работы с треками</param>
        public AlbumsController(ITrackService trackService)
        {
            _trackService = trackService;
        }

        /// <summary>
        /// Получение списка треков альбома
        /// </summary>
        /// <param name="id">Идентификатор альбома</param>
        /// <returns>Список треков</returns>
        // GET api/albums/5/tracks
        [HttpGet("{id}/tracks")]
        public IEnumerable<TrackViewModel> Get(int id)
        {
            return _trackService.GetTracksForAlbum(id).ToList().ConvertAll(new Converter<Track, TrackViewModel>(Converter.ToViewModel));
        }
    }
}