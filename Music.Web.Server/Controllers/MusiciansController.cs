﻿namespace Music.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.AspNetCore.Mvc;
    using Music.Domain;
    using Music.Domain.Models;
    using Music.Domain.Services.Interfaces;
    using Music.Domain.ViewModels;

    /// <summary>
    /// Контроллер для работы с исполнителями
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class MusiciansController : ControllerBase
    {
        private readonly IService<Musician> _musicianService;
        private readonly ITrackService _trackService;

        /// <summary>
        /// Конструктор класса <see cref="MusiciansController"/>
        /// </summary>
        /// <param name="musicianService">Сервис для работы с исполнителями</param>
        /// <param name="trackService">Сервис для работы с треками</param>
        public MusiciansController(IService<Musician> musicianService, ITrackService trackService)
        {
            _musicianService = musicianService;
            _trackService = trackService;
        }

        /// <summary>
        /// Получение списка всех исполнителей с альбомами в виде дерева
        /// </summary>
        /// <returns>Дерево исполнителей с альбомами</returns>
        // GET: api/musicians
        [HttpGet]
        public object Get()
        {
            var baseUrl = HttpContext.Request.Scheme + "://" + HttpContext.Request.Host.ToUriComponent();

            var musicians = _musicianService.GetAll().ToList().Select(m => new
            {
                id = m.Id.ToString(),
                label = m.Name + ", " + m.Genre + ", " + m.CareerStartYear,
                url = $"{baseUrl}/api/musicians/{m.Id}/tracks",
                nodes = m.Albums.Select(a => new
                {
                    id = $"{m.Id}_{a.Id}",
                    label = a.Name + ", " + a.ReleaseYear,
                    url = $"{baseUrl}/api/albums/{a.Id}/tracks"
                })
            });

            var root = new
            {
                id = "0",
                label = "All musicians",
                url = $"{baseUrl}/api/tracks",
                nodes = musicians
            };

            return root;
        }

        /// <summary>
        /// Получение списка треков исполнителя
        /// </summary>
        /// <param name="id">Идентификатор исполнителя</param>
        /// <returns>Список треков</returns>
        // GET api/musicians/5/tracks
        [HttpGet("{id}/tracks")]
        public IEnumerable<TrackViewModel> Get(int id)
        {
            return _trackService.GetTracksForMusician(id).ToList().ConvertAll(new Converter<Track, TrackViewModel>(Converter.ToViewModel));
        }
    }
}