﻿namespace Music.Web.Server.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.AspNetCore.Mvc;
    using Music.Domain;
    using Music.Domain.Models;
    using Music.Domain.Services.Interfaces;
    using Music.Domain.ViewModels;

    /// <summary>
    /// Контроллер для работы с треками
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class TracksController : ControllerBase
    {
        private readonly ITrackService _trackService;

        /// <summary>
        /// Конструктор класса <see cref="TracksController"/>
        /// </summary>
        /// <param name="trackService">Сервис для работы с треками</param>
        public TracksController(ITrackService trackService)
        {
            _trackService = trackService;
        }

        /// <summary>
        /// Получение списка всех треков
        /// </summary>
        /// <returns>Список треков</returns>
        // GET: api/tracks
        [HttpGet]
        public IEnumerable<TrackViewModel> Get()
        {
            return _trackService.GetAll().ToList().ConvertAll(new Converter<Track, TrackViewModel>(Converter.ToViewModel));
        }

        /// <summary>
        /// Изменение статуса "Избранное" для трека по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор</param>
        /// <param name="status">Статус</param>
        // PUT api/tracks/5/isfavourite/false
        [HttpPut("{id}/isFavourite/{status}")]
        public void IsFavouriteStatus(int id, bool status)
        {
            _trackService.SetIsFavouriteStatus(id, status);
        }

        /// <summary>
        /// Изменение статуса "Прослушан" для трека по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор</param>
        /// <param name="status">Статус</param>
        // PUT api/tracks/5/islistened/false
        [HttpPut("{id}/isListened/{status}")]
        public void IsListenedStatus(int id, bool status)
        {
            _trackService.SetIsListenedStatus(id, status);
        }

        /// <summary>
        /// Изменение рейтинга для трека по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор</param>
        /// <param name="rating">Рейтинг</param>
        // PUT api/tracks/5/rating/0
        [HttpPut("{id}/rating/{rating}")]
        public void Rating(int id, short rating)
        {
            _trackService.SetRating(id, rating);
        }
    }
}